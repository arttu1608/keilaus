﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApplication1;



namespace ConsoleApplication1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void CanRollGutterGame()
        {
            var game = new keilaus();
            for (var i = 0; i < 20; i++)
                game.Roll(0);
            Assert.AreEqual(0, game.Score);
        }


        [TestMethod]
        public void CanRollAllOnes()
        {
            var game = new keilaus();
            for (var i = 0; i < 20; i++)
                game.Roll(1);
            Assert.AreEqual(20, game.Score);
        }
        [TestMethod]
        public void CanRollSpare()
        {
            var game = new keilaus();
            game.Roll(5);
            game.Roll(5);
            game.Roll(3);
            for (var i = 0; i < 17; i++)
                game.Roll(0);
            Assert.AreEqual(16, game.Score);
        }
        [TestMethod]
        public void CanRollStrike()
        {
            var game = new keilaus();
            game.Roll(10);
            game.Roll(3);
            game.Roll(4);
            for (var i = 0; i < 16; i++)
                game.Roll(0);
            Assert.AreEqual(24, game.Score);
        }
            [TestMethod]
        public void CanRollPerfuct()
        {
            var game = new keilaus();
            for (var i = 0; i < 12; i++)
                game.Roll(10);
            Assert.AreEqual(300, game.Score); }
        } }

